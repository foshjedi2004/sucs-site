#!/bin/sh

# Setup script for SUCS website in users' public_html dirs
echo "SUCS website setup script"

# (try and) make sure we're running this from the right place
# SUCS website
# ^ a nasty kludge cause I don't know how to do it more sensibly
# test for the magic phrase on line 7 of this very file
head -n 7 setup.sh 2> /dev/null | tail -n 1 | grep -q "SUCS website"
if [ $? -ne 0 ]; then echo "Please run this script from its containing directory"; exit; fi

# generate correct .htaccess
echo -ne "\n\nGenerating .htaccess file..."
sed -e "s/\\\$USERNAME/$USER/" userhtaccess > htdocs/.htaccess
echo "Done!"

# generate settings file
echo -n "Generating settings file..."
sed -e "s:\\\$BASEDIR:$PWD:;s/\\\$USERNAME/$USER/" settingstemplate > htdocs/settings.php
echo "Done!"

# place a link in public_html
echo -n "Linking to public_html..."
ln -s $PWD/htdocs $HOME/public_html/sucssite
echo "Done!"

# and you're done!
echo -e "\nYou should now be able to view your local copy of the SUCS site at http://sucs.org/~$USER/sucssite/"
echo "Please contact a member of the admin team if you require use of the development database"

