<h3>I don't see anything!</h3>
<ul>
<li>If you don't see anything at all - no video player and no video - then you probably need to install a Flash player<br />You will normally want to install <a href="http://get.adobe.com/flashplayer/" target="_blank">Adobe Flash Player</a>, although there are alternatives that may work - these have not been tested</li>
</ul>
<h3>I see the video player, but it just sits there spinning!</h3>
<p>There are a few reasons for this:</p>
<ul>
<li>We might not be streaming anything<br />In the lead up to a planned stream then there should be a static information display, but this may disappear during testing. At other times then there may be no video. Check both quality options, or try playing the stream externally if you have a compatible media player.<br /></li>
<li>You may need to clear your browser cache - Instructions for most major browsers are <a href="http://www.wikihow.com/Clear-Your-Browser%27s-Cache">available here</a></li>
<li>If you see short bursts of video followed by the spinner then you may not have sufficient bandwidth for the stream - try the <a href="../../Community/Stream/Low">low version</a></li>
</ul>
<p>If you are still having problems, visit <a href="../../Community/Milliways">Milliways </a>and ask for an <a href="../../About/Staff">admin</a>, or email admin@</p>