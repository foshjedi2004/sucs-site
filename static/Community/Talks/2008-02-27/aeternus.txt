<div class="box">
<div class="boxhead">
<h2>Matthew Gwynne (aeternus) - "LaTeX Shiny"</h2>
</div>
<div class="boxcontent">
<p>Matt tells us all about some advanced LaTeX features for adding graphics.</p>
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2008-02-27/aeternus.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2008-02-27/aeternus.flv" />
<param name="image" value="/videos/talks/2008-02-27/aeternus.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2008-02-27/aeternus.png" />
</object>
</div>
<p><strong>Length: </strong>15m 58s</p>
<p><strong>Video: </strong><a href="/videos/talks/2008-02-27/aeternus.ogv" title="720x576 Ogg Theora - 78MB">720x576</a> (Ogg Theora, 78MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
