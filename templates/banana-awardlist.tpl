<ul class="bananas">
{foreach name=awards from=$awards item=award}
<li class="{award_image_class number=$award.score}">
	{if !isset($singlemember)}
	<strong>{if $award.score >0}+{/if}{$award.score}</strong> awarded to 
	<a href="{$baseurl}{if $award.real == TRUE}/Community/Members/{else}/Community/Bananas{/if}{$award.username}">{$award.username}</a> 
	<small>(from {$award.who}, {$award.whn|date_format:"%d %b %Y"})</small><br /> {/if}
	<q>{$award.why}</q> <br />
	{if isset($singlemember)} <small>
	<strong>{if $award.score >0}+{/if}{$award.score}</strong> awarded by {$award.who} on {$award.whn|date_format:"%d %b %Y"}
	</small>{/if}
</li>
{/foreach}
</ul>
