{* {php}
$script = "<script type=\"text/javascript\">\n";
$script .= "window.addEventListener('load', function(e) {\n";
$script .= "\tdocument.getElementById(\"uri\").focus();\n";
$script .= "}, true);\n";
$script .= "</script>";

$this->append('extra_scripts', $script);
{/php}
*}

{if $session->loggedin}

<div class="box" style="width: 80%; margin: auto;">
<div class="boxhead"><h2>Create a ShortURI</h2></div>
<div class="boxcontent">
<form class="admin" action="{$path}" method="POST">
        <div class="row">
                <label for="uri">URI</label>
                <span class="textinput"><input type="text" name="uri" id="uri" value="" style="width: 100%;" /></span>
        </div>
        <div class="row">
                <span class="textinput">
                        <input type="submit" id="submit-1" name="action" value="Create" />
                </span>
        </div>
        <div class="clear"></div>
</form>
</div>
<div class="hollowfoot"><div><div></div></div></div>
</div>
{/if}

{if $uri}<p>Your ShortURI is <a href="{$uri}">{$uri}</a></p>{/if}
