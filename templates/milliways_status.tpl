<table>
<tr>
 <td nowrap="nowrap" class="date" colspan="3">
Year: {foreach from=$yearlist item=year}{if $year != $thisyear}<a href="{$mwpath}/Status/{$year}-01-01">{$year}</a> {else}{$year} {/if}{/foreach}
 </td>
</tr>
<tr>
 <td nowrap="nowrap" class="date" colspan="3">
Month: {foreach from=$monthlist key=num item=month}{if $num != $thismonth}<a href="{$mwpath}/Status/{$thisyear}-{$num}-01">{$month}</a> {else}{$month} {/if}{/foreach}
 </td>
</tr>
{* do whole months for now
<tr>
 <td nowrap="nowrap" class="date" colspan="3">
Day: {foreach from=$daylist item=day}{if $day != $thisday}<a href="{$mwpath}/Status/{$thisyear}-{$thismonth}-{$day}">{$day}</a> {else}{$day} {/if}{/foreach}
 </td>
</tr>
*}
{assign var=lastday value=''}
{foreach from=$statuslist item=row name=list}
{if $row.day != $lastday}
{assign var=lastday value=$row.day}
<tr>
 <td nowrap="nowrap" class="date" colspan="3">{$row.added|date_format:'%d %b %Y'}</td>
</tr>
{/if}
{assign var=rows value=1}
{if $row.title}{assign var=rows value=$rows+1}{/if}
{if count($row.taglist)}{assign var=rows value=$rows+1}{/if}
<tr>
 <td nowrap="nowrap" class="time" rowspan="{$rows}">{$row.hour}</td>
 <td ><a href="{$mwpath}/Status/user/{$row.user|escape:'url'}">{$row.user|escape:'htmlall'}</a> {$row.doing|escape:'htmlall'}</td>
</tr>
{/foreach}
</table>
