	<div id="primary">
	<div id="primaryC">

		<div class="box">
<div class="boxhead"><h2>Freshers 2006</h2></div>

<div class="boxcontent">
<p>The Computer Society welcomes all Swansea freshers (and returning students)!</p>
<p>SUCS is one of the university's longest-running and largest societies. In addition to providing members with a range of <a href="http://sucs.org/services/about.php">IT services</a>, we hold regular events and socials throughout the year.</p>
<p>We have our own <a href="http://sucs.org/services/room.php">computer room</a> on campus with 24 hour swipe-card access. There are usually members present through the week so feel free to stop by.</p>
<p>As usual, we'll be at the freshers' fayre this year to answer any questions you may have.</p>
<p>If we miss you at the fayre, then please come meet us at a social. We're usually in the quiet side of JC's from about 1pm on Fridays :-)</p>
<p>Have a great year!</p>
</div>

<div class="boxfoot"><p>Posted by talyn256 at 11:53  on Tuesday August 15th 2006</p></div>
		</div>

		<div class="box">
<div class="boxhead"><h2>Jabber accounts on the new server</h2></div>

<div class="boxcontent">
<p>As part of our transition to a new server, we will be using different software for our Jabber service which uses the same authentication method as most of the other services.</p>
<p>In particular, this means:</p>
<ul>
<li>All members will automatically get Jabber accounts - no need to register.</li>
<li>Your Jabber password will be the same as your other passwords, i.e., the one you use to log in to the workstations in the room and check your email. If your client saves your password, remember to change it when you first connect to the new server.</li>
<li>Non-members will lose their Jabber accounts.</li>
<li>Any members whose Jabber username is different to their system username will ALSO lose their accounts. E.g., if your email address is johnsmith@sucs.org and your Jabber address is john@sucs.org, you'll lose the latter (but you will automatically get the Jabber address johnsmith@sucs.org).</li>
</ul>

<p>If the last point will affect you, please send
an email to admin@sucs.org telling us your system and Jabber usernames and
we'll see that your data gets transferred to your system username.</p>

</div>

<div class="boxfoot"><p>Posted by pwb at 22:51 on Saturday August 12th 2006</p></div>
		</div>


		<h2>Lorem ipsum dolor sit amet</h2>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla sit amet eros. Morbi volutpat quam. Donec faucibus lacus ac tortor. Cras gravida lectus at felis. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus aliquet magna sed elit. Curabitur urna velit, aliquet ut, luctus non, auctor eu, elit. Quisque id lectus. Suspendisse ligula massa, sodales a, tempus ut, molestie sed, purus. Duis ac nulla ut felis gravida porta. Nullam faucibus est et diam. Suspendisse imperdiet massa vitae odio. Mauris id quam. Vivamus nibh. Phasellus porttitor, nisl a lacinia egestas, pede magna vulputate libero, non tincidunt metus elit sit amet urna. Quisque ultricies turpis at sapien.</p>
		<p>Integer porta sapien ac erat. Maecenas fermentum suscipit tortor. Vestibulum in tortor. Sed lacus magna, interdum in, ullamcorper non, adipiscing quis, tortor. Sed pede magna, bibendum id, mattis vitae, dapibus sit amet, orci. Maecenas id sapien. Proin non risus at libero lacinia facilisis. Praesent sed nisl vitae ligula suscipit lobortis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam nunc arcu, rhoncus non, feugiat in, venenatis a, turpis. Donec varius. Quisque lobortis dolor iaculis nunc.</p>
		<h3 style="clear: both;">Nulla sit amet eros</h3>
		<p>Nam dictum felis et ipsum. Fusce et purus. Vestibulum non augue. In lacus odio, dignissim eget, placerat sit amet, luctus at, tortor. Nulla id felis quis eros mattis eleifend. Nulla elementum egestas risus. Fusce vel mi. Donec id elit. Nulla egestas consequat turpis. Nam pellentesque vestibulum felis. Fusce vitae mauris ac lectus congue cursus.</p>
		<p>Mauris eleifend, nulla eget facilisis ullamcorper, velit erat dignissim est, sed mollis neque felis tempor nibh. Etiam nunc. Sed rhoncus, massa quis tincidunt tincidunt, nibh lectus dictum lorem, sed laoreet eros leo in urna. Praesent in velit. Donec sed ipsum nec dolor consectetuer ultrices. Cras libero dui, ultrices sed, sagittis quis, iaculis a, mi. Proin semper lobortis enim. Phasellus sapien turpis, tristique ac, feugiat ut, lobortis elementum, enim. Nulla vel libero non nisi facilisis rhoncus. Vivamus vitae neque eget arcu suscipit accumsan.</p>
		<p>Praesent laoreet, felis vitae fermentum fringilla, quam mi porta nisi, id venenatis justo est et eros. Aliquam erat volutpat. Vivamus non risus. Nullam nunc. Aenean scelerisque lacinia urna. Suspendisse ac nulla. Nullam tristique. Praesent lorem odio, fringilla ut, euismod eu, adipiscing et, urna. Praesent elementum, turpis ut suscipit mattis, orci ipsum adipiscing lectus, sed ultrices pede quam nec enim. Ut id erat. Nunc sed turpis quis eros condimentum congue.</p>



	</div>
	</div>

	<div id="secondary">

		<div class="cbb">
			<h3>The heading</h3>
			<p>The content ... test test test</p>
		</div>
	</div>

