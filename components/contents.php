<?php

function listdir($mypath, $recurse, $summaries) {
	global $DB, $base;

	$dirlist = @scandir($base."static".$mypath);
	$output = array();
	if (is_array($dirlist)) foreach($dirlist as $file) {
		// Hide files starting with a .
		if ($file[0]!=".") {
			$element = array();
			if (!is_dir($base."static".$mypath."/".$file)) {
				$pagename = preg_replace("/\.txt/", "", $file);
				if ($summaries) $element['summary'] = $DB->GetOne("select summary from static where path=?", array($mypath."/".$pagename));
				if ($pagename!=@$lastdir) $output[$pagename]=str_replace("_", " ", $element);
			} else {
				if ($recurse) $element['file']=listdir($mypath."/".$file, $recurse, $summaries);
				if ($summaries) $element['summary'] = $DB->GetOne("select summary from static where path=?", array($mypath."/".$file));
				$output[$file]=$element;
				$lastdir=$file;
			}
		}
	}
	return $output;
}

if ($path==$component['path']) {
	$dirlist = listdir($component['path'], TRUE, TRUE);

	$smarty->assign('rootnode', $component['path']);
	$smarty->assign('dirlist', $dirlist);
	$output = $smarty->fetch('contents.tpl');
	$smarty->assign('title', $pathlist[1]);
	$smarty->assign('body', $output);
} else {
	include("static.php");

	if ($title!="File not found") {
		if (!is_dir($base."static".$path)) {
			$pos = strrpos($path, "/");
			$path = substr($path, 0, $pos);
		}

		$dirlist = listdir($path, FALSE, FALSE);
		$smarty->assign('rootnode', $path);
		$smarty->assign('data', $dirlist);
		$smarty->assign('level', 0);
		if (!isset($_REQUEST['action'])) {
			$output = $smarty->fetch('contents-loop.tpl');
			$smarty->assign("secondary", "<div class=\"cbb\"><h3>In this section</h3>".$output."</div>");
		}
	}
}
?>
