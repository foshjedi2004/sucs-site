<?php

/* Banana Admin library. This handles awarding bananas and nominating people to receive bananas */

// Requires $who variable to be initialised before including
if (!isset($who)) {
	echo "Error: I don't know who to award bananas to";
	$who = "nobody";
}

// Adding variable to limit maximum bananas awarded at once?

$maxbanana = 3;

// what permission does a user need to be able to award bananas?
$permission = "bananas";

// e-mail address to send banana nominations to
$contact = "bananas@sucs.org";

// banana admin bits
if (isset($session->groups[$permission])) {
	$smarty->assign("banana_admin", TRUE);

	if ($_REQUEST['action'] == "award") {
		if (trim ($_POST['why']) !== "") {
			$number = intval($_POST['number']);

			$why = $_POST['why'];

			if ( (abs($number) <= $maxbanana ) && $number ){

				$DB->Query("INSERT INTO awards (username, score, whn, who, why) VALUES (?,?,NOW(),?,?)", array($who, $number, $session->username, $why));
				header("Location: " . $_SERVER['PHP_SELF']);

			}
			// Checks for a nonzero banana and denies awarding if it's zero
			elseif ( $number == 0 ){
				trigger_error("Come on ".$session->username.", enter a nonzero value please");
			}

			else {
				trigger_error("Nice try ".$session->username."! You've submitted a request with too many bananas");
				}

		} else {
			trigger_error("No reason entered for the awarding of bananas.", E_USER_WARNING);
		}
	}
} elseif ($session->loggedin) {
	if ($_REQUEST['action'] == "award") {
		$number = intval($_POST['number']);
		$why = $_POST['why'];

	// Send a mail to someone about them bananas
		$msgbody = $session->username." thinks $who deserves $number bananas:\r\n";
		$msgbody .= "\"$why\"\r\n\r\nVisit https://sucs.org/Community/Members/$who if you want to make an award.";
		mail($contact, "Banana Award Nomination", $msgbody, $msgheaders);
		$smarty->assign("awarded", TRUE);

	}
}


$secondary = $smarty->getTemplateVars("secondary");
$secondary .= $smarty->fetch('banana-award.tpl');
$smarty->assign('secondary', $secondary);

$smarty->assign('who', "$who");


?>
